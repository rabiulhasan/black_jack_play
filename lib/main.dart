import 'dart:math';

import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: BlackJack(),
      ),
    );
  }
}

class BlackJack extends StatefulWidget {
  const BlackJack({Key? key}) : super(key: key);

  @override
  State<BlackJack> createState() => _BlackJackState();
}

class _BlackJackState extends State<BlackJack> {
  int leftCardNumber = 1;
  int rightCardNumber = 2;
  int playerResult = 0;
  int computerResult = 0;
  int _totalResult = 0;
  var message = "";
  bool _isButtonDisabled = false;

  void Calculation() {
    setState(() {
      leftCardNumber = Random().nextInt(13) + 1;
      rightCardNumber = Random().nextInt(13) + 1;

      if (leftCardNumber > rightCardNumber) {
        playerResult = playerResult + 1;
      } else if (rightCardNumber > leftCardNumber) {
        computerResult = computerResult + 1;
      }
      else {
        playerResult = playerResult + 0;
        computerResult = computerResult + 0;
      }

      _totalResult = playerResult + computerResult;
      if (_totalResult == 10) {
        _isButtonDisabled = true;
        if (playerResult > computerResult) {
          message = 'Player is win:$playerResult';
          //print('Player is win:$playerResult');
        }
        else if (computerResult > playerResult) {
          message = 'Computer is win:$computerResult';
          //print('Computer is win:$computerResult');
        }
        else {
          message = 'Game is Draw';
        }
      }
      // print(leftCardNumber);
      //print('$rightCardNumber');
    });
  }



  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/bg.jpg"),
          fit: BoxFit.cover,
        ),

      ),
      child: SafeArea(
        child: Column(
          children: [
            Expanded(child:  Image.asset(
              "assets/images/logo1.png",
              height: 125,
              width: 125,

            ),),

            Row(
              children: [
                Expanded(
                    child:
                    Image.asset("assets/images/card$leftCardNumber.png")),

                Expanded(
                    child:
                    Image.asset("assets/images/card$rightCardNumber.png")),
              ],
            ),
            //InkWell & GestureDetector both are same but inkwell something highlights
            /*  GestureDetector(
              onTap: () {
                setState(() {
                  leftCardNumber =Random().nextInt(14)+1;
                  rightCardNumber =Random().nextInt(14)+1;
                });

                if(leftCardNumber>rightCardNumber)
                {
                  playerResult=playerResult+1;
                }
                else{
                  computerResult=computerResult+1;
                }

                print(leftCardNumber);
                print('$rightCardNumber');
              },
              child: Image.asset(
                "assets/images/deal.png",
                height: 125,
                width: 125,
              ),
            ),*/
            TextButton(
              onPressed: _isButtonDisabled ? null : Calculation,
              child: Image.asset(
                "assets/images/deal.png",
                height: 70,
                width: 125,
              ),
            ),


            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Column(
                  children: [
                    Text(
                      "Palyer",
                      style: TextStyle(fontSize: 25),
                    ),
                    Text(
                      '$playerResult',
                      style: TextStyle(fontSize: 25),
                    )
                  ],
                ),
                Column(
                  children: [
                    Text(
                      "Computer",
                      style: TextStyle(fontSize: 25),
                    ),
                    Text(
                      '$computerResult',
                      style: TextStyle(fontSize: 25),
                    )
                  ],
                ),
              ],
            ),
            Text(
              'The time of Played game: $_totalResult',
              style: TextStyle(fontSize: 25),
            ),
            Text(
              message,
              style: TextStyle(
                fontSize: 30,
                backgroundColor: Colors.red,
              ),
            ),
           /* TextButton(
              onPressed:(){
                Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => BlackJack()), // this mainpage is your page to refresh
                      (Route<dynamic> route) => false,
                );
              },

              child: Icon(Icons.refresh),
            ),*/
            FloatingActionButton(onPressed:(){
              Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => BlackJack()), // this mainpage is your page to refresh
                    (Route<dynamic> route) => false,
              );
            },
              child: const Icon(Icons.refresh),
            )
          ],
        ),
      ),
    );
  }
}
